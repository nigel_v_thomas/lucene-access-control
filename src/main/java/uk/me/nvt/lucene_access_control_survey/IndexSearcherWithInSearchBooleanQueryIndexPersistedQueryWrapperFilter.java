package uk.me.nvt.lucene_access_control_survey;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexReaderContext;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;

/**
 * User: nigelthomas
 */
public class IndexSearcherWithInSearchBooleanQueryIndexPersistedQueryWrapperFilter extends IndexSearcher {

    public static final String DOCNAME = IndexUtility.INDEX_FIELD_DOCNAME;
    private IndexField indexField = null;

    public IndexSearcherWithInSearchBooleanQueryIndexPersistedQueryWrapperFilter(IndexReader r) {
        super(r);
    }

    public IndexSearcherWithInSearchBooleanQueryIndexPersistedQueryWrapperFilter(IndexReader r, IndexField indexField) {
        super(r);
        this.indexField = indexField;

    }

    public IndexSearcherWithInSearchBooleanQueryIndexPersistedQueryWrapperFilter(IndexReader r, ExecutorService executor) {
        super(r, executor);
    }

    public IndexSearcherWithInSearchBooleanQueryIndexPersistedQueryWrapperFilter(IndexReaderContext context, ExecutorService executor) {
        super(context, executor);
    }

    public IndexSearcherWithInSearchBooleanQueryIndexPersistedQueryWrapperFilter(IndexReaderContext context) {
        super(context);
    }

    @Override
    public TopDocs search(Query query, Filter filter, int n) throws IOException {
//        System.out.printf("Applying post search query, with allowed docs size %d %n", allowedRoles.size());
        Objects.requireNonNull(indexField, "indexFields must be set");

        if(filter!=null){
            throw new IllegalArgumentException("Expected filter input to be null..");
        }
        filter = new QueryWrapperFilter((new TermQuery(new Term(indexField.getFieldName(), indexField.getFieldValue()))));

        TopDocs searchResults = super.search(query, filter, n);
        ScoreDoc[] scoreDocs = searchResults.scoreDocs;
        List<ScoreDoc> docs = new ArrayList<>();
        float maxScore = Float.MIN_NORMAL;
        for (ScoreDoc scoreDoc : scoreDocs){
            Document doc = this.doc(scoreDoc.doc);
            String docname = doc.get(DOCNAME);
            docs.add(scoreDoc);
            maxScore = (maxScore < scoreDoc.score) ? scoreDoc.score : maxScore;
        }
        ScoreDoc[] scoreDocsOut = docs.toArray(new ScoreDoc[docs.size()]);
//        System.out.printf("%s %n", scoreDocs.length);
        return new TopDocs(scoreDocsOut.length, scoreDocsOut, maxScore);
    }
}
