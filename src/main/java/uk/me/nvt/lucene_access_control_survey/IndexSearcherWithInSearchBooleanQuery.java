package uk.me.nvt.lucene_access_control_survey;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexReaderContext;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;

/**
 * User: nigelthomas
 */
public class IndexSearcherWithInSearchBooleanQuery extends IndexSearcher {

    public static final String DOCNAME = IndexUtility.INDEX_FIELD_DOCNAME;
    private List<String> allowedDocs = new ArrayList<>();

    public IndexSearcherWithInSearchBooleanQuery(IndexReader r) {
        super(r);
    }

    public IndexSearcherWithInSearchBooleanQuery(IndexReader r, List<String> allowedDocs) {
        super(r);
        this.allowedDocs = allowedDocs;
        Objects.requireNonNull(allowedDocs, "allowedDocs must be set");

    }

    public IndexSearcherWithInSearchBooleanQuery(IndexReader r, ExecutorService executor) {
        super(r, executor);
    }

    public IndexSearcherWithInSearchBooleanQuery(IndexReaderContext context, ExecutorService executor) {
        super(context, executor);
    }

    public IndexSearcherWithInSearchBooleanQuery(IndexReaderContext context) {
        super(context);
    }

    @Override
    public TopDocs search(Query query, Filter filter, int n) throws IOException {
//        System.out.printf("Applying post search query, with allowed docs size %d %n", allowedDocs.size());

        Iterator iterator = allowedDocs.iterator();


        BooleanQuery.setMaxClauseCount(Integer.MAX_VALUE);

        BooleanQuery outerClause = new BooleanQuery();

        outerClause.add(query, BooleanClause.Occur.MUST);

        BooleanQuery booleanClauses = new BooleanQuery();
        while (iterator.hasNext()) {
            String doc = (String) iterator.next();
            booleanClauses.add(new TermQuery(new Term(IndexUtility.INDEX_FIELD_DOCNAME, doc)), BooleanClause.Occur.SHOULD);
        }
        outerClause.add(booleanClauses, BooleanClause.Occur.MUST);

        TopDocs searchResults = super.search(outerClause, filter, n);
        ScoreDoc[] scoreDocs = searchResults.scoreDocs;
        float maxScore = Float.MIN_NORMAL;
        for (ScoreDoc scoreDoc : scoreDocs){
            Document doc = this.doc(scoreDoc.doc);
            String docname = doc.get(DOCNAME);
            if (!allowedDocs.contains(docname)) {
                throw new IllegalStateException("Unexpected document found in result set has gone wrong :" + docname);
            }
//            System.out.printf(" search complete with query: %s %n query plan : %s %n", outerClause, super.explain(outerClause, scoreDoc.doc));
        }
        return new TopDocs(scoreDocs.length, scoreDocs, maxScore);
    }
}
