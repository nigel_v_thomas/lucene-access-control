package uk.me.nvt.lucene_access_control_survey;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

/**
 * User: nigelthomas
 */
public class XMLUtil {
    private final ThreadLocal<DocumentBuilder> builderTL = new ThreadLocal<DocumentBuilder>();
    private final ThreadLocal<XPath> xpathTL = new ThreadLocal<XPath>();

    public DocumentBuilder getXmlDocumentBuilder() {
        try {
            DocumentBuilder builder = builderTL.get();
            if (builder == null) {
                builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                builderTL.set(builder);
            }
            return builder;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public XPath getXpath() {
        try {
            XPath xpath = xpathTL.get();
            if (xpath == null) {
                xpath = XPathFactory.newInstance().newXPath();
                xpathTL.set(xpath);
            }
            return xpath;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
