package uk.me.nvt.lucene_access_control_survey;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexReaderContext;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;

/**
 * User: nigelthomas
 */
public class IndexSearcherWithPostSearchFilteringExhaustive extends IndexSearcher {

    public static final String DOCNAME = IndexUtility.INDEX_FIELD_DOCNAME;
    private Set<String> allowedDocs = new TreeSet<>();

    public IndexSearcherWithPostSearchFilteringExhaustive(IndexReader r) {
        super(r);
    }

    public IndexSearcherWithPostSearchFilteringExhaustive(IndexReader r, Set<String> allowedDocs) {
        super(r);
        this.allowedDocs = allowedDocs;
        Objects.requireNonNull(allowedDocs, "allowedDocs must be set");

    }

    public IndexSearcherWithPostSearchFilteringExhaustive(IndexReader r, ExecutorService executor) {
        super(r, executor);
    }

    public IndexSearcherWithPostSearchFilteringExhaustive(IndexReaderContext context, ExecutorService executor) {
        super(context, executor);
    }

    public IndexSearcherWithPostSearchFilteringExhaustive(IndexReaderContext context) {
        super(context);
    }

    @Override
    public TopDocs search(Query query, Filter filter, int n) throws IOException {
//        System.out.printf("Applying post search query, with allowed docs size %d %n", allowedDocs.size());
        List<ScoreDoc> filteredScoreDocs = new ArrayList<>();
        int fetchDocLength = 99999;
        TopDocs searchResults = super.search(query, filter, fetchDocLength);
        float maxScore = Float.MIN_NORMAL;
        int iterationOfSearch = 0;

        while (n > filteredScoreDocs.size()) {
            ScoreDoc[] scoreDocs = searchResults.scoreDocs;
            ScoreDoc lastScoreDoc = null;

            for (ScoreDoc scoreDoc : scoreDocs){
                lastScoreDoc = scoreDoc;
                Document doc = this.doc(scoreDoc.doc);
                String docname = doc.get(DOCNAME);
                if (allowedDocs.contains(docname) && n > filteredScoreDocs.size()) {
                    filteredScoreDocs.add(scoreDoc);
                    maxScore = (maxScore < scoreDoc.score) ? scoreDoc.score : maxScore;
                }
            }

            // exit loop if the search results is less than n (request number of documents)
            if (scoreDocs.length < n || scoreDocs.length < 1) {
                break;
            }

            // page next set of results from search
            searchResults = super.searchAfter(lastScoreDoc, query, filter, fetchDocLength);

            System.out.printf("Search iteration %d completed, filterDocSize %d %n", ++iterationOfSearch, filteredScoreDocs.size());
        }


        ScoreDoc[] scoreDocsFiltered = filteredScoreDocs.toArray(new ScoreDoc[filteredScoreDocs.size()]);
//        System.out.printf("inital array size %d, array size after filter %d %n", scoreDocs.length, scoreDocsFiltered.length);
        return new TopDocs(scoreDocsFiltered.length, scoreDocsFiltered, maxScore);
    }
}
