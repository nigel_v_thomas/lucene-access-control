package uk.me.nvt.lucene_access_control_survey;

import java.util.List;

public class IndexField {
    private final String fieldName;
    private final String fieldValue;
    private final List<String> documentsToUpdate;

    public IndexField(String fieldName, String fieldValue, List<String> documentsToUpdate) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.documentsToUpdate = documentsToUpdate;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    @Override
    public String toString() {
        return "IndexField{" +
                "fieldName='" + fieldName + '\'' +
                ", fieldValue='" + fieldValue + '\'' +
                ", documentsToUpdate=" + documentsToUpdate +
                '}';
    }

    public List<String> getDocumentsToUpdate() {
        return documentsToUpdate;
    }
}
