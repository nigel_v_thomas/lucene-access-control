package uk.me.nvt.lucene_access_control_survey;

/**
 * User: nigelthomas
 */
public interface IndexUtility {
    public static final String ID_FIELD = "docid";
    public static final String INDEX_FIELD_ROLES = "roles";
    public static final String INDEX_FIELD_DOCNAME = "docname";
}
