package uk.me.nvt.lucene_access_control_survey;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexReaderContext;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;

/**
 * User: nigelthomas
 */
public class IndexSearcherWithInSearchBooleanQueryIndexPersisted extends IndexSearcher {

    public static final String DOCNAME = IndexUtility.INDEX_FIELD_DOCNAME;
    private IndexField indexField = null;

    public IndexSearcherWithInSearchBooleanQueryIndexPersisted(IndexReader r) {
        super(r);
    }

    public IndexSearcherWithInSearchBooleanQueryIndexPersisted(IndexReader r, IndexField indexField) {
        super(r);
        this.indexField = indexField;

    }

    public IndexSearcherWithInSearchBooleanQueryIndexPersisted(IndexReader r, ExecutorService executor) {
        super(r, executor);
    }

    public IndexSearcherWithInSearchBooleanQueryIndexPersisted(IndexReaderContext context, ExecutorService executor) {
        super(context, executor);
    }

    public IndexSearcherWithInSearchBooleanQueryIndexPersisted(IndexReaderContext context) {
        super(context);
    }

    @Override
    public TopDocs search(Query query, Filter filter, int n) throws IOException {
        Objects.requireNonNull(indexField, "indexFields must be set");

        String booleanExclusionQuery = "";

        System.setProperty("org.apache.lucene.maxClauseCount", Integer.toString(Integer.MAX_VALUE));

        BooleanQuery.setMaxClauseCount(Integer.MAX_VALUE);

        BooleanQuery outerClause = new BooleanQuery();

        outerClause.add(query, BooleanClause.Occur.MUST);

        BooleanQuery booleanClauses = new BooleanQuery();

        booleanClauses.add(new TermQuery(new Term(indexField.getFieldName(), indexField.getFieldValue())), BooleanClause.Occur.SHOULD);

        outerClause.add(booleanClauses, BooleanClause.Occur.MUST);

//        System.out.printf("%s %n", outerClause.toString());


        TopDocs searchResults = super.search(outerClause, filter, n);
        ScoreDoc[] scoreDocs = searchResults.scoreDocs;
        List<ScoreDoc> docs = new ArrayList<>();
        float maxScore = Float.MIN_NORMAL;
//        System.out.printf("query result set size %d %n", scoreDocs.length);
        for (ScoreDoc scoreDoc : scoreDocs){
            Document doc = this.doc(scoreDoc.doc);
            String docname = doc.get(DOCNAME);
            docs.add(scoreDoc);
            maxScore = (maxScore < scoreDoc.score) ? scoreDoc.score : maxScore;
        }
        ScoreDoc[] scoreDocsOut = docs.toArray(new ScoreDoc[docs.size()]);
//        System.out.printf("%s %n", scoreDocs.length);
        return new TopDocs(scoreDocsOut.length, scoreDocsOut, maxScore);
    }
}
