package uk.me.nvt.lucene_access_control_survey;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;

/**
 * User: nigelthomas
 */
public class TwoPassRelevancyScorer extends IndexSearcher {

    private String indexField;
    private IndexSearcher indexSearcher;

    private TwoPassRelevancyScorer(IndexReader r) {
        super(r);
    }


    private TwoPassRelevancyScorer(IndexReader r, ExecutorService executor, IndexSearcher indexSearcher) {
        super(r, executor);
        this.indexSearcher = indexSearcher;
    }

    private TwoPassRelevancyScorer(IndexReaderContext context, ExecutorService executor, IndexSearcher indexSearcher) {
        super(context, executor);
        this.indexSearcher = indexSearcher;
    }

    private TwoPassRelevancyScorer(IndexReader r, ExecutorService executor) {
        super(r, executor);
    }

    private TwoPassRelevancyScorer(IndexReaderContext context, ExecutorService executor) {
        super(context, executor);
    }

    private TwoPassRelevancyScorer(IndexReaderContext context) {
        super(context);
    }

    public TwoPassRelevancyScorer(IndexSearcher indexSearcher, String indexField) {
        super(indexSearcher.getIndexReader());
        Objects.requireNonNull(indexSearcher, "allowedDocs must be set");
        this.indexSearcher = indexSearcher;
        this.indexField = indexField;

    }

    @Override
    public TopDocs search(Query query, Filter filter, int n) throws IOException {
        AllDocsCollector allDocsCollector = new AllDocsCollector();
        super.search(query, filter, allDocsCollector);
        IndexWriterConfig conf = new IndexWriterConfig(Version.LUCENE_41, new StandardAnalyzer(Version.LUCENE_41));

        // setup a ramDirectory, add search result from first search into it
        // perform search on restricted set and re-evaluate the rankings to the restricted set
        TopDocs searchResults;
        try (
                Directory ramDirectory = new RAMDirectory();
                IndexWriter indexWriter = new IndexWriter(ramDirectory, conf);
        ) {
            indexWriter.commit();

            Map<String, ScoreDoc> docMap = new HashMap<>();
            for (ScoreDoc scoreDoc : allDocsCollector.getHits()) {
                Document doc = indexSearcher.doc(scoreDoc.doc);
                IndexableField indexableField = doc.getField(indexField);
                docMap.put(indexableField.stringValue(), scoreDoc);
                indexWriter.addDocument(doc);
            }
            indexWriter.commit();

            IndexReader indexReader = DirectoryReader.open(ramDirectory);
            IndexSearcher memIndexSearcher = new IndexSearcher(indexReader);

            TopScoreDocCollector memTopDocsCollector = TopScoreDocCollector.create(2000, true);

            memIndexSearcher.search(query, filter, memTopDocsCollector);
            TopDocs memTopDocs = memTopDocsCollector.topDocs();
            List<ScoreDoc> scoreDocsList = new ArrayList<>();
            for (ScoreDoc memTopDoc : memTopDocs.scoreDocs) {
                Document doc = memIndexSearcher.doc(memTopDoc.doc);
                IndexableField docnameField = doc.getField(indexField);
                String docName = docnameField.stringValue();
                ScoreDoc scoreDoc = docMap.get(docName);
                scoreDocsList.add(new ScoreDoc(scoreDoc.doc,memTopDoc.score, scoreDoc.shardIndex));
            }
            searchResults = new TopDocs(memTopDocs.totalHits, scoreDocsList.toArray(new ScoreDoc[0]), memTopDocs.getMaxScore());

        }
        return searchResults;
    }

    static class AllDocsCollector extends Collector{
        List<ScoreDoc> docs = new ArrayList<ScoreDoc>();
        private Scorer scorer;
        private int docBase;

        @Override
        public void setScorer(Scorer scorer) throws IOException {
            this.scorer = scorer;
        }

        @Override
        public void collect(int doc) throws IOException {
            docs.add(new ScoreDoc(doc+docBase, scorer.score()));
        }

        @Override
        public void setNextReader(AtomicReaderContext context) throws IOException {
            this.docBase = context.docBase;
        }

        @Override
        public boolean acceptsDocsOutOfOrder() {
            return true;
        }

        public void reset() {
            docs.clear();
        }

        public List<ScoreDoc> getHits() {
            return docs;
        }
    }

}
