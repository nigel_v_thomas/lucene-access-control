package uk.me.nvt.lucene_access_control_survey;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexReaderContext;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;

/**
 * User: nigelthomas
 */
public class IndexSearcherWithPostSearchFiltering extends IndexSearcher {

    public static final String DOCNAME = IndexUtility.INDEX_FIELD_DOCNAME;
    private List<String> allowedDocs = new ArrayList<>();

    public IndexSearcherWithPostSearchFiltering(IndexReader r) {
        super(r);
    }

    public IndexSearcherWithPostSearchFiltering(IndexReader r, List<String> allowedDocs) {
        super(r);
        this.allowedDocs = allowedDocs;
        Objects.requireNonNull(allowedDocs, "allowedDocs must be set");

    }

    public IndexSearcherWithPostSearchFiltering(IndexReader r, ExecutorService executor) {
        super(r, executor);
    }

    public IndexSearcherWithPostSearchFiltering(IndexReaderContext context, ExecutorService executor) {
        super(context, executor);
    }

    public IndexSearcherWithPostSearchFiltering(IndexReaderContext context) {
        super(context);
    }

    @Override
    public TopDocs search(Query query, Filter filter, int n) throws IOException {
//        System.out.printf("Applying post search query, with allowed docs size %d %n", allowedDocs.size());
        TopDocs searchResults = super.search(query, filter, n);
        ScoreDoc[] scoreDocs = searchResults.scoreDocs;
        List<ScoreDoc> filteredScoreDocs = new ArrayList<>();
        float maxScore = Float.MIN_NORMAL;
        for (ScoreDoc scoreDoc : scoreDocs){
            Document doc = this.doc(scoreDoc.doc);
            String docname = doc.get(DOCNAME);
            if (allowedDocs.contains(docname)) {
                filteredScoreDocs.add(scoreDoc);
                maxScore = (maxScore < scoreDoc.score) ? scoreDoc.score : maxScore;
            }
        }
        ScoreDoc[] scoreDocsFiltered = filteredScoreDocs.toArray(new ScoreDoc[filteredScoreDocs.size()]);
//        System.out.printf("inital array size %d, array size after filter %d %n", scoreDocs.length, scoreDocsFiltered.length);
        return new TopDocs(scoreDocsFiltered.length, scoreDocsFiltered, maxScore);
    }
}
