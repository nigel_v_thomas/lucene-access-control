Package : uk.me.nvt.uk.me.nvt.lucene_access_control_survey

This package, contains a set of test exploring current access control implementation strategies for Lucene.
In each test we setup the base environment for the experiment, to begin we create three documents,
each containing some terms as follows:

1.  DOC1 contains a single term, TERM1
2.  DOC2 has two occurrences of the term, TERM1
3.  DOC3 has a single term TERM2

All the above documents are accessible to the user searching, in addition to these documents we pad the index
with numOfDocs-3 hidden documents. We then measure the ratio of numOfDocs vs ratio of scores, this is
output in a text file, we then create a log log plot of the results and assert if the resulting graph shows a linear
relationship. If it does, then it is clear that this approach is vulnerable to the TF/IDF exploit.